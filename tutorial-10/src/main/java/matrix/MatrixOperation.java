package matrix;

public class MatrixOperation {

    public static double[][] basicMultiplicationAlgorithm(
            double[][] firstMatrix, double[][] secondMatrix)
            throws InvalidMatrixSizeForMultiplicationException {
        int numberOfRowInFirstMatrix = firstMatrix.length;
        int numberOfColumnInFirstMatrix = firstMatrix[0].length;


        int numberOfRowInSecondMatrix = secondMatrix.length;
        int numberOfColumnInSecondMatrix = secondMatrix[0].length;

        if (numberOfColumnInFirstMatrix != numberOfRowInSecondMatrix) {
            throw new InvalidMatrixSizeForMultiplicationException();
        }
        double[][] matrixResult =
                new double[numberOfRowInFirstMatrix][numberOfColumnInSecondMatrix];

        for (int i = 0; i < numberOfRowInFirstMatrix; i++) {
            for (int j = 0; j < numberOfColumnInSecondMatrix; j++) {
                for (int k = 0; k < numberOfColumnInFirstMatrix; k++) {
                    matrixResult[i][j] += firstMatrix[i][k] * secondMatrix[k][j];
                }
            }
        }
        return matrixResult;
    }

    /**
     * Redesign version of Strassen Algoritmh that enable it to do matrix multiplication
     * for any positive integer orde.
     * @param a first matrix with n x n orde
     * @param b second matrix with m x m orde, and n = m
     * @return the result of matrix multiplication
     */
    public static double[][] strassenMatrixMultiForNonSquareMatrix(double[][] a, double[][] b) {
        int newLengtha = StrassensAlgorithm.powerMatrix(a.length);
        int newLengthb = StrassensAlgorithm.powerMatrix(b.length);

        double[][] newa = new double[newLengtha][newLengtha];
        double[][] newb = new double[newLengthb][newLengthb];
        double[][] cret = new double[a.length][a.length];
        for (int x = 0; x < a.length; x++) {
            for (int y = 0; y < a.length; y++) {
                newa[x][y] = a[x][y];
            }
        }

        for (int x = 0; x < b.length; x++) {
            for (int y = 0; y < b.length; y++) {
                newb[x][y] = b[x][y];
            }
        }
        double[][] c = StrassensAlgorithm.strassenMultiplicationAlgorithm(newa, newb);
        for (int x = 0; x < cret.length; x++) {
            for (int y = 0; y < cret.length; y++) {
                cret[x][y] = c[x][y];
            }
        }
        return cret;
    }
}