package sorting;

public class Finder {


    /**
     * Some searching algorithm that possibly the slowest algorithm.
     * This algorithm can search a value irregardless of whether the sequence already sorted or not.
     * @param arrOfInt is a sequence of integer.
     * @param searchedValue value that need to be searched inside the sequence.
     * @return -1 if there are no such value inside the sequence, else return searchedValue.
     */
    public static int slowSearch(int[] arrOfInt, int searchedValue) {
        int returnValue = -1;

        for (int element : arrOfInt) {
            if (element == searchedValue) {
                returnValue = element;
            }
        }

        return returnValue;
    }

    /**
     * Some searching algorithm that possibly the fastest algorithm.
     * This algorightm only works when the sequence already sorted.
     * @param arrOfInt is a sequence of sorted integer.
     * @param searchedValue value that need to be searched inside the sequence.
     * @return -1 if there are no such value inside the sequence, else return searchedValue.
     */
    public static int fastSearch(int[] arrOfInt, int searchedValue) {
        int l, r, m;
        l = 0;
        r = arrOfInt.length - 1;
        while (l <= r) {
            m = (l + r) / 2;
            if (arrOfInt[m] == searchedValue) {
                return searchedValue;
            } else if (arrOfInt[m] < searchedValue) {
                l = m + 1;
            } else {
                r = m - 1;
            }
        }
        return -1;
    }
}
