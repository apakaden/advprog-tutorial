package sorting;


public class Sorter {

    /**
     * Some sorting algorithm that possibly the slowest algorithm.
     *
     * @param inputArr array of integer that need to be sorted.
     * @return a sorted array of integer.
     */

    public static int[] slowSort(int[] inputArr) {
        int temp;
        for (int i = 1; i < inputArr.length; i++) {
            for (int j = i; j > 0; j--) {
                if (inputArr[j] < inputArr[j - 1]) {
                    temp = inputArr[j];
                    inputArr[j] = inputArr[j - 1];
                    inputArr[j - 1] = temp;
                }
            }
        }
        return inputArr;
    }

    /**
     * Some sorting algorithm that possible the fastest algorithm.
     *
     * @param inputArr array of integer that need to be sorted.
     * @return a sorted array of integer.
     */
    public static int[] fastSort(int[] inputArr) {
        int length = inputArr.length, left_length, right_length;
        if (length <= 1) {
            return inputArr;
        }

        left_length = length / 2;
        right_length = length - left_length;

        int[] left, right, merged;
        left = new int[left_length];
        right = new int[right_length];
        merged = new int[length];
        int l = 0, r = 0, m = 0;
        for (int i = 0; i < length; i++) {
            if (l < left_length) {
                left[l++] = inputArr[i];
            } else {
                right[r++] = inputArr[i];
            }
        }

        left = fastSort(left);
        right = fastSort(right);
        l = 0;
        r = 0;
        m = 0;
        while (l < left_length && r < right_length) {
            if (left[l] < right[r]) {
                merged[m++] = left[l++];
            } else {
                merged[m++] = right[r++];
            }
        }
        for (int i = r; i < right_length; i++) {
            merged[m++] = right[i];
        }
        for (int i = l; i < left_length; i++) {
            merged[m++] = left[i];
        }
        return merged;
    }

}
