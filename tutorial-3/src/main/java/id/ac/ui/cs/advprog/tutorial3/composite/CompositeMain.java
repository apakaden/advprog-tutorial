package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;

public class CompositeMain {
    public static void main(String[] args) {
        Company myCompany = new Company();
        Ceo myCeo = new Ceo("CEO", 1000000);
        Cto myCto = new Cto("CEO", 500000);

        myCompany.addEmployee(myCeo);
        System.out.printf("Add CEO to myCompany\n");
        myCompany.addEmployee(myCto);
        System.out.printf("ADD CTO to myCompany\n");

        System.out.printf("Total salaries of myCompany = %.2f\n", myCompany.getNetSalaries());
    }
}
