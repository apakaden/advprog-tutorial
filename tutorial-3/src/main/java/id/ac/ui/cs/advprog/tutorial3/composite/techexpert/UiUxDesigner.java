package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class UiUxDesigner extends Employees {
    public UiUxDesigner(String name, double salary) {
        this.name = name;
        this.salary = salary;
        this.role = "UI/UX Designer";
        if (salary < 90000.00) {
            throw new IllegalArgumentException("UI/UX Designer salary must "
                    + "not lower than 90000.00");
        }
    }
}
