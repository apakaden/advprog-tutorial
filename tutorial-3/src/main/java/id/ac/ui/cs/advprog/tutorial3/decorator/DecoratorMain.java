package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class DecoratorMain {
    public static void printFoodCondition(Food food) {
        System.out.printf("Description: %s Cost: %.2f\n", food.getDescription(), food.cost());
    }

    public static void main(String[] args) {
        Food myBurger = BreadProducer.THICK_BUN.createBreadToBeFilled();
        printFoodCondition(myBurger);

        myBurger = FillingDecorator.BEEF_MEAT.addFillingToBread(myBurger);
        printFoodCondition(myBurger);

        myBurger = FillingDecorator.TOMATO_SAUCE.addFillingToBread(myBurger);
        printFoodCondition(myBurger);
    }
}
