package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

public class DepokPizzaStoreTest {
    DepokPizzaStore dpPizzaStore;

    @Before
    public void setUp() {
        dpPizzaStore = new DepokPizzaStore();
    }

    @Test
    public void createPizzaTest() {
        Pizza pizza;

        pizza = dpPizzaStore.createPizza("cheese");
        assertEquals("Depok Style Cheese Pizza", pizza.getName());

        pizza = dpPizzaStore.createPizza("veggie");
        assertEquals("Depok Style Veggie Pizza", pizza.getName());

        pizza = dpPizzaStore.createPizza("clam");
        assertEquals("Depok Style Clam Pizza", pizza.getName());
    }

}
