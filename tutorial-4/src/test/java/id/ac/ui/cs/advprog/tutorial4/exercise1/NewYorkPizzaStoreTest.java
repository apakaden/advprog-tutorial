package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

public class NewYorkPizzaStoreTest {
    NewYorkPizzaStore nyPizzaStore;

    @Before
    public void setUp() {
        nyPizzaStore = new NewYorkPizzaStore();
    }

    @Test
    public void testCreatePizza() {
        Pizza pizza;

        pizza = nyPizzaStore.createPizza("cheese");
        assertEquals("New York Style Cheese Pizza", pizza.getName());

        pizza = nyPizzaStore.createPizza("veggie");
        assertEquals("New York Style Veggie Pizza", pizza.getName());

        pizza = nyPizzaStore.createPizza("clam");
        assertEquals("New York Style Clam Pizza", pizza.getName());
    }

}
