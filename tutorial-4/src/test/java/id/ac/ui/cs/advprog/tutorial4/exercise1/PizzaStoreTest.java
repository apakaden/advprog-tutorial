package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class PizzaStoreTest {
    @Spy
    PizzaStore pizzaStore;

    @Mock
    Pizza pizza;

    @Before
    public void setUp() {
        when(pizzaStore.createPizza("pizza")).thenReturn(pizza);
    }

    @Test
    public void testOrderPizza() {
        assertEquals(pizza, pizzaStore.orderPizza("pizza"));
    }
}
