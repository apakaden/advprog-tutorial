package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class ClamPizzaTest extends PizzaTest {
    private ClamPizza clamPizza;

    @Before
    @Override
    public void setUp() {
        super.setUp();
        when(clamMock.toString()).thenReturn("Mock clam");
        when(pizzaIngredientFactoryMock.createClam()).thenReturn(clamMock);
        clamPizza = new ClamPizza(pizzaIngredientFactoryMock);
        clamPizza.setName("Clam pizza");
    }

    @Test
    public void testPrepare() {
        clamPizza.prepare();
        assertEquals(clamPizza.dough.toString(), doughMock.toString());
        assertEquals(clamPizza.sauce.toString(), sauceMock.toString());
        assertEquals(clamPizza.cheese.toString(), cheeseMock.toString());
        assertEquals(clamPizza.clam.toString(), clamMock.toString());
    }

}