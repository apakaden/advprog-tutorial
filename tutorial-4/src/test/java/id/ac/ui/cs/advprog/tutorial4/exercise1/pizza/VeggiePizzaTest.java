package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class VeggiePizzaTest extends PizzaTest {
    private VeggiePizza veggiePizza;

    @Before
    @Override
    public void setUp() {
        super.setUp();
        veggiesMockArray = new Veggies[3];
        veggiesMockArray[0] = veggiesMock;
        veggiesMockArray[1] = veggiesMock;
        veggiesMockArray[2] = veggiesMock;
        when(pizzaIngredientFactoryMock.createVeggies()).thenReturn(veggiesMockArray);

        veggiePizza = new VeggiePizza(pizzaIngredientFactoryMock);
        veggiePizza.setName("Veggie pizza");
    }

    @Test
    public void testPrepare() {
        veggiePizza.prepare();
        assertEquals(veggiePizza.dough.toString(), doughMock.toString());
        assertEquals(veggiePizza.sauce.toString(), sauceMock.toString());
        assertEquals(veggiePizza.cheese.toString(), cheeseMock.toString());
        assertEquals(veggiePizza.veggies.toString(), veggiesMockArray.toString());
    }

}