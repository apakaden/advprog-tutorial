package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CurriculumVitaeController {

    @GetMapping("/curriculum-vitae")
    public String show(@RequestParam(name = "visitor", required = false, defaultValue = "")
                               String visitor, Model model) {

        String title = visitor.equals("") ? "This is my CV" : visitor
                + ", I hope you interested to hire me";
        model.addAttribute("title", title);
        return "curriculum_vitae";
    }

}
