import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Objects;

import org.junit.Before;
import org.junit.Test;



public class MovieTest {

    private Movie movie;
    private Movie movie2;

    @Before
    public void setUp() {
        movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        movie2 = new Movie("Tebaatusasula", Movie.REGULAR);
    }

    @Test
    public void getTitle() {
        assertEquals("Who Killed Captain Alex?", movie.getTitle());
    }

    @Test
    public void setTitle() {
        movie.setTitle("Bad Black");
        assertEquals("Bad Black", movie.getTitle());
    }

    @Test
    public void getPriceCode() {
        assertEquals(Movie.REGULAR, movie.getPriceCode());
    }

    @Test
    public void setPriceCode() {
        movie.setPriceCode(Movie.CHILDREN);
        assertEquals(Movie.CHILDREN, movie.getPriceCode());
    }

    @Test
    public void equalsItself() {
        assertTrue(movie.equals(movie));
    }

    @Test
    public void equalsNull() {
        assertFalse(movie.equals(null));
    }

    @Test
    public void equalsAnotherDifferentMovie() {
        assertFalse(movie.equals(movie2));
    }

    @Test
    public void equalsAnotherSameMovie() {
        Movie movie3 = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        assertTrue(movie.equals(movie3));
    }

    @Test
    public void hash() {
        assertEquals(Objects.hash(movie.getTitle(), movie.getPriceCode()), movie.hashCode());
    }
}
