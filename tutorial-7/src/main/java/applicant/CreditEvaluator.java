package applicant;

public class CreditEvaluator extends EvaluatorChain {

    public CreditEvaluator(Evaluator next) {
        super(next);
        this.setPredicate(applicant -> applicant.getCreditScore() > 600);
    }
}