package applicant;

public class CriminalRecordsEvaluator extends EvaluatorChain {

    public CriminalRecordsEvaluator(Evaluator next) {
        super(next);
        this.setPredicate(applicant -> !applicant.hasCriminalRecord());
    }
}
