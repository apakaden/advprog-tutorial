package applicant;

public class EmploymentEvaluator extends EvaluatorChain {

    public EmploymentEvaluator(Evaluator next) {
        super(next);
        this.setPredicate(applicant -> applicant.getEmploymentYears() > 0);
    }
}
