package applicant;

import java.util.function.Predicate;

public class EvaluatorChain implements Evaluator {
    private Evaluator next;
    private Predicate<Applicant> predicate;

    public void setPredicate(Predicate<Applicant> predicate) {
        this.predicate = predicate;
    }

    @Override
    public Predicate<Applicant> getPredicate() {
        return predicate.and(next.getPredicate());
    }

    public EvaluatorChain(Evaluator nextEvaluator) {
        next = nextEvaluator;
    }
}