package applicant;

import java.util.function.Predicate;

public class QualifiedEvaluator implements Evaluator {

    @Override
    public Predicate<Applicant> getPredicate() {
        return applicant -> applicant.isCredible();
    }
}
