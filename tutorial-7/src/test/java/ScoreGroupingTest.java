import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class ScoreGroupingTest {
    Map<String, Integer> scores;
    Map<Integer, List<String>> namesByScore;

    @Before
    public void setUp() {
        scores = new HashMap<>();
        scores.put("A", 1);
        scores.put("B", 1);
        scores.put("C", 3);
        scores.put("D", 3);

        namesByScore = new HashMap<>();
        namesByScore.put(1, Arrays.asList("A", "B"));
        namesByScore.put(3, Arrays.asList("C", "D"));
    }

    @Test
    public void testScoreGrouping() {
        assertEquals(ScoreGrouping.groupByScores(scores), namesByScore);
    }
}