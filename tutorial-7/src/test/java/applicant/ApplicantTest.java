package applicant;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class ApplicantTest {
    Applicant applicant;
    Evaluator eval1;
    Evaluator eval2;
    Evaluator eval3;
    Evaluator eval4;

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(System.out);
    }

    @Before
    public void setUp() {
        applicant = new Applicant();
        eval1 = new CreditEvaluator(new QualifiedEvaluator());
        eval2 = new CreditEvaluator(new EmploymentEvaluator(
                new QualifiedEvaluator()));
        eval3 = new CriminalRecordsEvaluator(new EmploymentEvaluator(
                new QualifiedEvaluator()));
        eval4 = new CriminalRecordsEvaluator(new CreditEvaluator(
                new EmploymentEvaluator(new QualifiedEvaluator())));
    }

    @Test
    public void testPrintEvaluationTrue() {
        Applicant.printEvaluation(true);
        assertEquals("Result of evaluating applicant: accepted\n", outContent.toString());
    }

    @Test
    public void testPrintEvaluationFalse() {
        Applicant.printEvaluation(false);
        assertEquals("Result of evaluating applicant: rejected\n", outContent.toString());
    }

    @Test
    public void testIsCredible() {
        assertEquals(true, applicant.isCredible());
    }

    @Test
    public void testGetCreditScore() {
        assertEquals(700, applicant.getCreditScore());
    }

    @Test
    public void testGetEmploymentYears() {
        assertEquals(10, applicant.getEmploymentYears());
    }

    @Test
    public void testHasCriminalRecord() {
        assertEquals(true, applicant.hasCriminalRecord());
    }

    @Test
    public void testEval1() {
        assertEquals(true, eval1.getPredicate().test(applicant));
    }

    @Test
    public void testEval2() {
        assertEquals(true, eval2.getPredicate().test(applicant));
    }

    @Test
    public void testEval3() {
        assertEquals(false, eval3.getPredicate().test(applicant));
    }

    @Test
    public void testEval4() {
        assertEquals(false, eval4.getPredicate().test(applicant));
    }

    @Test
    public void testEvaluate() {
        assertEquals(true, Applicant.evaluate(applicant, eval1));
    }
}
