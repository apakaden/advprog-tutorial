package game;

import java.util.concurrent.atomic.AtomicInteger;

public class Scorer implements Runnable {
    private AtomicInteger score;
    private AtomicInteger timer;

    public Scorer() {
        score = new AtomicInteger(100);
        timer = new AtomicInteger(0);
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(1000);
                timer.incrementAndGet();
                score.decrementAndGet();
            } catch (InterruptedException e) {
                return;
            }
        }
    }

    public int getTime() {
        return timer.get();
    }

    public void addScore(int percentage) {
        score.addAndGet(score.get() * percentage / 100);
    }

    public int getScore() {
        return score.get();
    }
}
