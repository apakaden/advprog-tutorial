package tallycounter;

public class SynchronizedTallyCounter extends TallyCounter {
    private int counter = 0;

    @Override
    public synchronized void increment() {
        counter += 1;
    }

    @Override
    public synchronized void decrement() {
        counter -= 1;
    }

    @Override
    public synchronized int value() {
        return counter;
    }
}
