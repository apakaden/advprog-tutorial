package tutorial.javari;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import tutorial.javari.animal.Animal;
import tutorial.javari.animal.Condition;
import tutorial.javari.animal.Gender;


public class AnimalRecord {

    private int counter;
    private ArrayList<Animal> animals;

    public synchronized Animal addAnimal(String type, String name, String gender, double length,
                            double weight, String condition) {

        animals = AnimalCSVParser.read();
        if (animals.size() == 0) {
            counter = 0;
        } else {
            counter = animals.get(animals.size() - 1).getId();
        }

        Animal animal = new Animal(++counter,
                type, name, Gender.parseGender(gender),
                length, weight, Condition.parseCondition(condition));

        animals.add(animal);
        AnimalCSVParser.write(animals);
        return animal;
    }

    public synchronized ArrayList<Animal> getAnimals() {
        return AnimalCSVParser.read();
    }

    public synchronized Animal getAnimal(int id) {
        animals = AnimalCSVParser.read();
        List<Animal> selectedAnimal = animals.stream()
                .filter(animal -> animal.getId() == id)
                .collect(Collectors.toList());

        if (selectedAnimal.size() == 1) {
            return selectedAnimal.get(0);
        }
        return null;
    }

    public synchronized Animal deleteAnimal(int id) {
        animals = AnimalCSVParser.read();

        List<Animal> deletedAnimal = animals.stream()
                .filter(animal -> animal.getId() == id)
                .collect(Collectors.toList());

        if (deletedAnimal.size() == 1) {
            Animal deleted = deletedAnimal.get(0);
            animals.remove(deleted);
            AnimalCSVParser.write(animals);
            return deleted;
        }
        return null;
    }
}
