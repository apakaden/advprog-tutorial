package tutorial.javari;

import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import tutorial.javari.animal.Animal;
import tutorial.javari.animal.AnimalForm;

@RestController
public class JavariController {

    static final AnimalRecord animalRecord = new AnimalRecord();

    @GetMapping("/javari")
    public ResponseEntity getList() {
        ArrayList<Animal> animals = animalRecord.getAnimals();
        if (animals.size() == 0) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body("{\"message\":\"No animals found\"}");
        }
        return ResponseEntity.ok(animalRecord.getAnimals());
    }

    @GetMapping("/javari/{id}")
    public ResponseEntity getDetail(@PathVariable(value = "id") String id) {
        Animal animal = animalRecord.getAnimal(Integer.parseInt(id));
        if (animal == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body("{\"message\":\"No animals found with given ID\"}");
        }
        return ResponseEntity.ok(animal);
    }

    @DeleteMapping("/javari/{id}")
    public ResponseEntity delete(@PathVariable(value = "id") String id) {
        Animal animal = animalRecord.deleteAnimal(Integer.parseInt(id));
        if (animal == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body("{\"message\":\"No animals found with given ID\"}");
        }
        return ResponseEntity.ok(animal);
    }

    @PostMapping("/javari")
    public ResponseEntity create(@RequestBody AnimalForm animalForm) {
        Animal animal = animalRecord.addAnimal(animalForm.getType(), animalForm.getName(),
                animalForm.getGender(), animalForm.getLength(), animalForm.getWeight(),
                animalForm.getCondition());
        if (animal == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body("{\"message\":\"Bad Request\"}");
        }
        return ResponseEntity.ok(animal);
    }
}

